﻿using AoC2022.Engine;

namespace AoC2022.Days;

public static class Day5
{
	private static readonly List<string> Input1 = Loader.LoadInput("1");
	private static readonly List<string> InputExample = Loader.LoadInput("Example");

	public static string Run1()
	{
		var Input = Input1;

		#region Building stacks

		List<string> instructions = Input.Where(x => x.Contains("move")).ToList();
		var listCounter = Input[Input.Count - instructions.Count - 2].Split().Where(x => x != "").Select(x => Convert.ToInt32(x)).Max();
		Dictionary<int, List<char>> stacks = new();

		foreach (var iterator in Enumerable.Range(1, listCounter))
		{
			stacks[iterator] = new List<char>();
		}

		for (int row = Input.Count - instructions.Count - 2; row >= 0; row--)
		{
			for (int col = 0; col < Input[row].Length; col++)
			{
				var box = Input[row][col];

				if (char.IsLetter(box))
				{
					stacks[col / 4 + 1].Add(box);
				}
			}
		}

		#endregion

		foreach (var task in instructions)
		{
			var elements = task.Split(" ");
			var times = Convert.ToInt32(elements[1]);
			var from = Convert.ToInt32(elements[3]);
			var to = Convert.ToInt32(elements[5]);

			foreach (var timesIterator in Enumerable.Range(1, times))
			{
				var tempBox = stacks[from][^1];
				stacks[from].RemoveAt(stacks[from].Count - 1);
				stacks[to].Add(tempBox);
			}
		}

		var lasts = string.Join("", stacks.Select(x => x.Value).Select(x => x.ElementAt(x.Count - 1)));

		return lasts;
	}

	public static string Run2()
	{
		var Input = Input1;

		#region Building stacks

		List<string> instructions = Input.Where(x => x.Contains("move")).ToList();
		var listCounter = Input[Input.Count - instructions.Count - 2].Split().Where(x => x != "").Select(x => Convert.ToInt32(x)).Max();
		Dictionary<int, List<char>> stacks = new();

		foreach (var iterator in Enumerable.Range(1, listCounter))
		{
			stacks[iterator] = new List<char>();
		}

		for (int row = Input.Count - instructions.Count - 2; row >= 0; row--)
		{
			for (int col = 0; col < Input[row].Length; col++)
			{
				var box = Input[row][col];

				if (char.IsLetter(box))
				{
					stacks[col / 4 + 1].Add(box);
				}
			}
		}

		#endregion

		foreach (var task in instructions)
		{
			var elements = task.Split(" ");
			var times = Convert.ToInt32(elements[1]);
			var from = Convert.ToInt32(elements[3]);
			var to = Convert.ToInt32(elements[5]);

			var tempList = new List<char>(stacks[from]);
			tempList.Reverse();
			var tempBoxes = tempList.Take(times).ToList();
			tempBoxes.Reverse();

			foreach (var VARIABLE in Enumerable.Range(0, times))
			{
				stacks[from].RemoveAt(stacks[from].Count - 1);
			}

			stacks[to].AddRange(tempBoxes);
		}

		var lasts = string.Join("", stacks.Select(x => x.Value).Select(x => x.ElementAt(x.Count - 1)));

		return lasts;
	}
}