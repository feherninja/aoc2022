﻿using AoC2022.Engine;

namespace AoC2022.Days;

public static class Day4
{
	private static readonly List<string> Input1 = Loader.LoadInput("1");
	private static readonly List<string> Input2 = Loader.LoadInput("2");

	public static string Run1()
	{
		int encompassCounter = 0;

		foreach (var task in Input1)
		{
			var task1from = Convert.ToInt32(task.Split(",")[0].Split("-")[0]);
			var task1to = Convert.ToInt32(task.Split(",")[0].Split("-")[1]);
			var task2from = Convert.ToInt32(task.Split(",")[1].Split("-")[0]);
			var task2to = Convert.ToInt32(task.Split(",")[1].Split("-")[1]);

			if ((task1from <= task2from && task2to <= task1to) || task2from <= task1from && task1to <= task2to)
			{
				encompassCounter++;
			}
		}

		return encompassCounter.ToString();
	}

	public static string Run2()
	{
		int overlap = 0;

		foreach (var task in Input1)
		{
			var task1from = Convert.ToInt32(task.Split(",")[0].Split("-")[0]);
			var task1to = Convert.ToInt32(task.Split(",")[0].Split("-")[1]);
			var task2from = Convert.ToInt32(task.Split(",")[1].Split("-")[0]);
			var task2to = Convert.ToInt32(task.Split(",")[1].Split("-")[1]);

			if ((task1from <= task2from && task2from <= task1to)
			    || (task1from <= task2to && task2to <= task1to)
			    || (task2from <= task1from && task1from <= task2to)
			    || (task2from <= task1to && task1to <= task2to))
			{
				overlap++;
			}
		}

		return overlap.ToString();
	}
}