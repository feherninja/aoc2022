﻿using AoC2022.Engine;

namespace AoC2022.Days;

public static class Day10
{
	private static readonly List<string> Input = Loader.LoadInput("");
	private static readonly List<string> InputExample = Loader.LoadInput("Example");
	private static readonly List<string> InputMin = Loader.LoadInput("Min");

	public static string Run1()
	{
		var input = Input;
		int cycle = 1;
		int registerX = 1;
		List<int> trackedCycles = new List<int>() { 20, 60, 100, 140, 180, 220 };
		Dictionary<int, int> signalStrength = new Dictionary<int, int>();

		foreach (var command in input)
		{
			if (trackedCycles.Contains(cycle))
			{
				signalStrength[cycle] = cycle * registerX;
			}

			if (command == "noop")
			{
				cycle++;
			}
			else
			{
				cycle++;

				if (trackedCycles.Contains(cycle))
				{
					signalStrength[cycle] = cycle * registerX;
				}

				registerX += Convert.ToInt32(command.Split(" ")[1]);
				cycle++;
			}
		}

		return signalStrength.Sum(x => x.Value).ToString();
	}

	public static string Run2()
	{
		var input = Input;
		int cycle = 1;
		int registerX = 1;
		int crtWidth = 40;

		List<int> litPixels = new List<int>();

		foreach (var command in input)
		{
			List<int> sprite = new List<int>() { registerX - 1, registerX, registerX + 1 };

			if (sprite.Contains(cycle % crtWidth - 1))
			{
				litPixels.Add(cycle - 1);
			}

			if (command == "noop")
			{
				cycle++;
			}
			else
			{
				cycle++;

				if (sprite.Contains(cycle % crtWidth - 1))
				{
					litPixels.Add(cycle - 1);
				}

				registerX += Convert.ToInt32(command.Split(" ")[1]);

				cycle++;
			}
		}

		// Display
		for (int i = 0; i < cycle - 1; i++)
		{
			if (i % crtWidth == 0)
			{
				Console.WriteLine();
			}

			Console.Write(litPixels.Contains(i) ? "#" : ".");
		}

		return string.Empty;
	}
}