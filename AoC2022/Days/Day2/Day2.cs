﻿using AoC2022.Engine;

namespace AoC2022.Days;

public static class Day2
{
	private static readonly List<string> Input1 = Loader.LoadInput("1");

	public static string Run1()
	{
		Dictionary<string, int> Points = new Dictionary<string, int>()
		{
			{ "A X", 1 + 3 },
			{ "A Y", 2 + 6 },
			{ "A Z", 3 + 0 },
			{ "B X", 1 + 0 },
			{ "B Y", 2 + 3 },
			{ "B Z", 3 + 6 },
			{ "C X", 1 + 6 },
			{ "C Y", 2 + 0 },
			{ "C Z", 3 + 3 }
		};

		return Input1.Select(x => Points[x]).Sum().ToString();
	}

	public static string Run2()
	{
		Dictionary<string, int> Points = new Dictionary<string, int>()
		{
			{ "A X", 3 + 0 },
			{ "A Y", 1 + 3 },
			{ "A Z", 2 + 6 },
			{ "B X", 1 + 0 },
			{ "B Y", 2 + 3 },
			{ "B Z", 3 + 6 },
			{ "C X", 2 + 0 },
			{ "C Y", 3 + 3 },
			{ "C Z", 1 + 6 }
		};

		return Input1.Select(x => Points[x]).Sum().ToString();
	}
}