﻿using AoC2022.Engine;

namespace AoC2022.Days;

public static class Day8
{
	private static readonly List<string> Input = Loader.LoadInput("");
	private static readonly List<string> InputExample = Loader.LoadInput("Example");

	public static string Run1()
	{
		var input = Input;
		int visibleTrees = 0;

		for (int i = 0; i < input.Count; i++)
		{
			for (int j = 0; j < input[i].Length; j++)
			{
				bool visibility = true;

				// not on the edge
				if (!(i == 0 || i == input.Count - 1 || j == 0 || j == input[0].Length - 1))
				{
					bool coveredLeft, coveredRight, coveredTop, coveredBottom;

					coveredLeft = input[i].AsSpan(0, j).ToString().Any(x => Convert.ToInt32(x.ToString()) >= Convert.ToInt32(input[i][j].ToString()));
					if (coveredLeft) visibility = false;

					if (!visibility)
					{
						coveredRight = input[i].AsSpan(j + 1, input[i].Length - j - 1).ToString().Any(x => Convert.ToInt32(x.ToString()) >= Convert.ToInt32(input[i][j].ToString()));
						if (!coveredRight) visibility = true;
					}

					if (!visibility)
					{
						coveredTop = input.Take(i).Select(x => x[j]).Any(x => Convert.ToInt32(x.ToString()) >= Convert.ToInt32(input[i][j].ToString()));
						if (!coveredTop) visibility = true;
					}

					if (!visibility)
					{
						coveredBottom = input.Skip(i + 1).Select(x => x[j]).Any(x => Convert.ToInt32(x.ToString()) >= Convert.ToInt32(input[i][j].ToString()));
						if (!coveredBottom) visibility = true;
					}
				}

				if (visibility)
				{
					visibleTrees++;
				}
			}
		}

		return visibleTrees.ToString();
	}

	public static string Run2()
	{
		var input = Input;
		int visibleTrees = 0;

		for (int i = 0; i < input.Count; i++)
		{
			for (int j = 0; j < input[i].Length; j++)
			{
				int leftTrees = 0;
				int rightTrees = 0;
				int topTrees = 0;
				int bottomTrees = 0;

				// check left
				for (int k = j - 1; k >= 0; k--)
				{
					if (Convert.ToInt32(input[i][k]) < Convert.ToInt32(input[i][j]))
					{
						leftTrees++;
					}
					else
					{
						leftTrees++;
						break;
					}
				}

				//check right 
				for (int k = j + 1; k < input[i].Length; k++)
				{
					if (Convert.ToInt32(input[i][k]) < Convert.ToInt32(input[i][j]))
					{
						rightTrees++;
					}
					else
					{
						rightTrees++;
						break;
					}
				}

				//check top
				for (int k = i - 1; k >= 0; k--)
				{
					if (Convert.ToInt32(input[k][j]) < Convert.ToInt32(input[i][j]))
					{
						topTrees++;
					}
					else
					{
						topTrees++;
						break;
					}
				}

				// check bottom
				for (int k = i + 1; k < input.Count; k++)
				{
					if (Convert.ToInt32(input[k][j]) < Convert.ToInt32(input[i][j]))
					{
						bottomTrees++;
					}
					else
					{
						bottomTrees++;
						break;
					}
				}

				var currentVisible = leftTrees * rightTrees * topTrees * bottomTrees;

				if (currentVisible > visibleTrees) visibleTrees = currentVisible;
			}
		}

		return visibleTrees.ToString();
	}
}