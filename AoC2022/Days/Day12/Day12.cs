﻿using AoC2022.Days.D12.Models;
using AoC2022.Engine;

namespace AoC2022.Days;

//
//     ██ ██    ██ ███████ ████████      ██████ ██   ██ ███████  ██████ ██   ██     ███████ ██   ██  █████  ███    ███ ███████    ██   ██ ██      ███████ ██   ██ 
//     ██ ██    ██ ██         ██        ██      ██   ██ ██      ██      ██  ██      ██      ██   ██ ██   ██ ████  ████ ██          ██ ██  ██      ██       ██ ██  
//     ██ ██    ██ ███████    ██        ██      ███████ █████   ██      █████       ███████ ███████ ███████ ██ ████ ██ █████        ███   ██      ███████   ███   
//██   ██ ██    ██      ██    ██        ██      ██   ██ ██      ██      ██  ██           ██ ██   ██ ██   ██ ██  ██  ██ ██          ██ ██  ██           ██  ██ ██  
// █████   ██████  ███████    ██         ██████ ██   ██ ███████  ██████ ██   ██     ███████ ██   ██ ██   ██ ██      ██ ███████ ██ ██   ██ ███████ ███████ ██   ██ 
//                                                                                                                                                                

public static class Day12
{
	private static readonly List<string> Input = Loader.LoadInput("");
	private static readonly List<string> InputExample = Loader.LoadInput("Example");

	public static string Run1()
	{
		var input = Input;
		List<Node> checkedNodes = new List<Node>();
		List<Node> susNodes = new List<Node>();
		int iteration = 0;

		Node start = new Node();
		start.X = input.Select((x, i) => new { value = x, number = i }).First(x => x.value.Contains('S')).number;
		start.Y = input[start.X].Select((x, i) => new { value = x, number = i }).First(x => x.value == 'S').number;
		start.Weight = 0;
		start.Elevation = 'a';
		susNodes.Add(start);

		Node endPoint = new Node();
		endPoint.X = input.Select((x, i) => new { value = x, number = i }).First(x => x.value.Contains('E')).number;
		endPoint.Y = input[endPoint.X].Select((x, i) => new { value = x, number = i }).First(x => x.value == 'E').number;
		endPoint.Elevation = 'z';
		input[endPoint.X] = input[endPoint.X].Replace('E', 'z');

		SetTargetDistance(start, endPoint);

		while (true)
		{
			iteration++;
			susNodes = susNodes.OrderBy(x => x.TargetDistance).ThenByDescending(x => x.Elevation).ToList();

			var susNode = susNodes[0];


			// Get possible next nodes

			//up
			// check if you can do better
			var checkBetter = susNodes.FirstOrDefault(x => x.X == susNode.X - 1 && x.Y == susNode.Y);

			if (checkBetter is not null)
			{
				if (checkBetter.Weight > susNode.Weight + 1)
				{
					checkBetter.Weight = susNode.Weight + 1;
				}
			}

			if (susNode.X > 0 &&
			    Convert.ToInt32(input[susNode.X - 1][susNode.Y]) <= Convert.ToInt32(susNode.Elevation) + 1 &&
			    !checkedNodes.Any(x => x.X == susNode.X - 1 && x.Y == susNode.Y) &&
			    !susNodes.Any(x => x.X == susNode.X - 1 && x.Y == susNode.Y))
			{
				var nextNode = new Node()
				{
					From = susNode,
					Elevation = input[susNode.X - 1][susNode.Y],
					Weight = susNode.Weight + 1,
					TargetDistance = SetTargetDistance(susNode.X - 1, susNode.Y, endPoint),
					X = susNode.X - 1,
					Y = susNode.Y
				};

				susNodes.Add(nextNode);
			}

			//down
			checkBetter = susNodes.FirstOrDefault(x => x.X == susNode.X + 1 && x.Y == susNode.Y);

			if (checkBetter is not null)
			{
				if (checkBetter.Weight > susNode.Weight + 1)
				{
					checkBetter.Weight = susNode.Weight + 1;
				}
			}

			if (susNode.X < input.Count - 1 &&
			    Convert.ToInt32(input[susNode.X + 1][susNode.Y]) <= Convert.ToInt32(susNode.Elevation) + 1 &&
			    !checkedNodes.Any(x => x.X == susNode.X + 1 && x.Y == susNode.Y) &&
			    !susNodes.Any(x => x.X == susNode.X + 1 && x.Y == susNode.Y))
			{
				var nextNode = new Node()
				{
					From = susNode,
					Elevation = input[susNode.X + 1][susNode.Y],
					Weight = susNode.Weight + 1,
					TargetDistance = SetTargetDistance(susNode.X + 1, susNode.Y, endPoint),
					X = susNode.X + 1,
					Y = susNode.Y
				};

				susNodes.Add(nextNode);
			}

			//left
			checkBetter = susNodes.FirstOrDefault(x => x.X == susNode.X && x.Y == susNode.Y - 1);

			if (checkBetter is not null)
			{
				if (checkBetter.Weight > susNode.Weight + 1)
				{
					checkBetter.Weight = susNode.Weight + 1;
				}
			}

			if (susNode.Y > 0 &&
			    Convert.ToInt32(input[susNode.X][susNode.Y - 1]) <= Convert.ToInt32(susNode.Elevation) + 1 &&
			    !checkedNodes.Any(x => x.X == susNode.X && x.Y == susNode.Y - 1) &&
			    !susNodes.Any(x => x.X == susNode.X && x.Y == susNode.Y - 1))
			{
				var nextNode = new Node()
				{
					From = susNode,
					Elevation = input[susNode.X][susNode.Y - 1],
					Weight = susNode.Weight + 1,
					TargetDistance = SetTargetDistance(susNode.X, susNode.Y - 1, endPoint),
					X = susNode.X,
					Y = susNode.Y - 1
				};

				susNodes.Add(nextNode);
			}

			//right
			checkBetter = susNodes.FirstOrDefault(x => x.X == susNode.X && x.Y == susNode.Y + 1);

			if (checkBetter is not null)
			{
				if (checkBetter.Weight > susNode.Weight + 1)
				{
					checkBetter.Weight = susNode.Weight + 1;
				}
			}

			if (susNode.Y < input[0].Length - 1 &&
			    Convert.ToInt32(input[susNode.X][susNode.Y + 1]) <= Convert.ToInt32(susNode.Elevation) + 1 &&
			    !checkedNodes.Any(x => x.X == susNode.X && x.Y == susNode.Y + 1) &&
			    !susNodes.Any(x => x.X == susNode.X && x.Y == susNode.Y + 1))
			{
				var nextNode = new Node()
				{
					From = susNode,
					Elevation = input[susNode.X][susNode.Y + 1],
					Weight = susNode.Weight + 1,
					TargetDistance = SetTargetDistance(susNode.X, susNode.Y + 1, endPoint),
					X = susNode.X,
					Y = susNode.Y + 1
				};

				susNodes.Add(nextNode);
			}

			// Check if any is END
			if (susNodes.Any(x => x.X == endPoint.X && x.Y == endPoint.Y))
			{
				var lastNode = susNodes.First(x => x.X == endPoint.X && x.Y == endPoint.Y);
				DisplayBoard(checkedNodes, susNodes, susNode, lastNode);
				return $"{lastNode.Weight.ToString()} - in {iteration} iteration";
			}

			// flush node to checkedNodes and remove from sus
			checkedNodes.Add(susNode);
			susNodes.Remove(susNode);
		}
	}

	private static void DisplayBoard(List<Node> checkedNodes, List<Node> susNodes, Node susNode, Node analyzeNode = null)
	{
		Console.Clear();
		List<Node> path = new List<Node>();

		if (analyzeNode is not null)
		{
			Node pointer = analyzeNode;

			while (pointer != null)
			{
				path.Add(pointer);
				pointer = pointer.From;
			}
		}

		var input = Input;

		int length = input[0].Length;
		int height = input.Count;
		int pathC = 0;

		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < length; j++)
			{
				Console.ForegroundColor = ConsoleColor.White;

				if (checkedNodes.Any(x => x.X == i && x.Y == j))
				{
					Console.ForegroundColor = ConsoleColor.DarkYellow;
				}

				if (susNodes.Any(x => x.X == i && x.Y == j))
				{
					Console.ForegroundColor = ConsoleColor.Green;
				}

				if (susNode.X == i && susNode.Y == j)
				{
					Console.ForegroundColor = ConsoleColor.Blue;
				}

				if (path.Any(x => x.X == i && x.Y == j))
				{
					pathC++;
					Console.ForegroundColor = ConsoleColor.DarkRed;
				}

				Console.Write(input[i][j]);
			}

			Console.WriteLine();
		}
	}

	private static void SetTargetDistance(Node start, Node end)
	{
		start.TargetDistance = Math.Abs(end.X - start.X) + Math.Abs(end.Y - start.Y);
	}

	private static int SetTargetDistance(int x, int y, Node end)
	{
		return Math.Abs(end.X - x) + Math.Abs(end.Y - y);
	}

	public static string Run2()
	{
		return string.Empty;
	}
}