﻿namespace AoC2022.Days.D12.Models;

public class Node
{
	public Node From { get; set; }
	public char Elevation { get; set; }
	public int Weight { get; set; }
	public int TargetDistance { get; set; }
	public int X { get; set; }
	public int Y { get; set; }
}