﻿namespace AoC2022.Days.Models;

public class Banana
{
	public Banana()
	{
		DivisionRemainder = new Dictionary<int, int>();
	}

	public int InitialValue { get; set; }
	public Dictionary<int, int> DivisionRemainder { get; set; }
}