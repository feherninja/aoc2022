﻿namespace AoC2022.Days.Models;

public class ScaryMonkey
{
	public ScaryMonkey()
	{
		Items = new List<Banana>();
	}

	public int InspectionCounter { get; set; }
	public List<Banana> Items { get; set; }
	public string Operation { get; set; }
	public int TestDivider { get; set; }
	public int TrueMonkeyIndex { get; set; }
	public int FalseMonkeyIndex { get; set; }
}