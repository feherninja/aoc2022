﻿namespace AoC2022.Days.Models;

public class Monkey
{
	public Monkey()
	{
		Items = new List<int>();
	}

	public int InspectionCounter { get; set; }
	public List<int> Items { get; set; }
	public string Operation { get; set; }
	public int TestDivider { get; set; }
	public int TrueMonkeyIndex { get; set; }
	public int FalseMonkeyIndex { get; set; }
}