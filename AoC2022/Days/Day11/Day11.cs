﻿using System.Linq.Dynamic.Core;
using AoC2022.Days.Models;
using AoC2022.Engine;
using myAlias = System.Linq.Dynamic;

namespace AoC2022.Days;

public static class Day11
{
	private static readonly List<string> Input = Loader.LoadInput("");
	private static readonly List<string> InputExample = Loader.LoadInput("Example");

	private static List<Monkey> ParseMonkeys()
	{
		var monkeys = new List<Monkey>();

		foreach (var monkey in Input.Chunk(7))
		{
			var mojojojo = new Monkey();

			foreach (var item in monkey[1].Split(":")[1].Split(","))
			{
				mojojojo.Items.Add(Convert.ToInt32(item));
			}

			mojojojo.Operation = monkey[2].Split("=")[1].Trim();

			mojojojo.TestDivider = Convert.ToInt32(monkey[3].Split(" ").Last());
			mojojojo.TrueMonkeyIndex = Convert.ToInt32(monkey[4].Split(" ").Last());
			mojojojo.FalseMonkeyIndex = Convert.ToInt32(monkey[5].Split(" ").Last());

			monkeys.Add(mojojojo);
		}

		return monkeys;
	}

	private static List<ScaryMonkey> ParseScaryMonkeys()
	{
		var monkeys = new List<ScaryMonkey>();

		foreach (var monkey in Input.Chunk(7))
		{
			var mojojojo = new ScaryMonkey();

			foreach (var item in monkey[1].Split(":")[1].Split(","))
			{
				mojojojo.Items.Add(new Banana() { InitialValue = Convert.ToInt32(item) });
			}

			mojojojo.Operation = monkey[2].Split("=")[1].Trim();

			mojojojo.TestDivider = Convert.ToInt32(monkey[3].Split(" ").Last());
			mojojojo.TrueMonkeyIndex = Convert.ToInt32(monkey[4].Split(" ").Last());
			mojojojo.FalseMonkeyIndex = Convert.ToInt32(monkey[5].Split(" ").Last());

			monkeys.Add(mojojojo);
		}

		var dividers = monkeys.Select(x => x.TestDivider).ToList();

		foreach (var monkey in monkeys)
		{
			foreach (var banana in monkey.Items)
			{
				foreach (var divider in dividers)
				{
					banana.DivisionRemainder[divider] = banana.InitialValue % divider;
				}
			}
		}

		return monkeys;
	}

	public static string Run1()
	{
		int rounds = 20;
		var monkeys = ParseMonkeys();

		foreach (var round in Enumerable.Range(0, rounds))
		{
			foreach (var monkey in monkeys)
			{
				foreach (var item in monkey.Items)
				{
					monkey.InspectionCounter++;
					var query = new int[] { 0 }.AsQueryable()
					                           .Select(monkey.Operation.Replace("old", $"{item}"));

					double itemNewValue = query.First();
					itemNewValue = Math.Floor(itemNewValue / 3);

					if (itemNewValue % monkey.TestDivider == 0)
					{
						monkeys[monkey.TrueMonkeyIndex].Items.Add((int) itemNewValue);
					}
					else
					{
						monkeys[monkey.FalseMonkeyIndex].Items.Add((int) itemNewValue);
					}
				}

				monkey.Items = new List<int>();
			}
		}

		var result = monkeys.OrderByDescending(x => x.InspectionCounter).Take(2).Select(x => x.InspectionCounter).Aggregate((x, y) => x * y);
		return result.ToString();
	}

	public static string Run2()
	{
		int rounds = 10000;
		var scaryMonkeys = ParseScaryMonkeys();

		foreach (var round in Enumerable.Range(0, rounds))
		{
			foreach (var monkey in scaryMonkeys)
			{
				foreach (var item in monkey.Items)
				{
					monkey.InspectionCounter++;

					// Inspect
					int operand;
					bool square = false;

					if (monkey.Operation.Split(" ").Last() != "old")
					{
						operand = Convert.ToInt32(monkey.Operation.Split(" ").Last());
					}
					else
					{
						square = true;
						operand = 1;
					}

					foreach (var key in item.DivisionRemainder.Keys)
					{
						if (monkey.Operation.Contains('+'))
						{
							item.DivisionRemainder[key] = (item.DivisionRemainder[key] + operand) % key;
						}
						else
						{
							// multiplication can flip off
							if (!square)
							{
								item.DivisionRemainder[key] = (item.DivisionRemainder[key] * operand) % key;
							}

							// birthing a porcupine on squaring
							else
							{
								item.DivisionRemainder[key] = ((int) Math.Pow(item.DivisionRemainder[key], 2)) % key;
							}
						}
					}

					// Check routing 
					if (item.DivisionRemainder[monkey.TestDivider] == 0)
					{
						scaryMonkeys[monkey.TrueMonkeyIndex].Items.Add(item);
					}
					else
					{
						scaryMonkeys[monkey.FalseMonkeyIndex].Items.Add(item);
					}
				}

				monkey.Items = new List<Banana>();
			}
		}

		var result = scaryMonkeys.OrderByDescending(x => x.InspectionCounter).Take(2).Select(x => (double) x.InspectionCounter).Aggregate((x, y) => x * y);
		return result.ToString();
	}
}