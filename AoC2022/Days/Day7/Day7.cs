﻿using AoC2022.Days.Models;
using AoC2022.Engine;

namespace AoC2022.Days;

public static class Day7
{
	private static readonly List<string> Input1 = Loader.LoadInput("1");
	private static readonly List<string> InputExample = Loader.LoadInput("Example");

	private static List<Node> StructureBuilder()
	{
		Node root = new Node("\\");
		Node pwd = root;
		Node sizePropagator = root;
		List<Node> allDirs = new List<Node>() { root };
		var input = Input1;

		foreach (var command in input.Skip(2))
		{
			if (command == "$ cd ..")
			{
				pwd = pwd.Parent;
			}
			else if (command.StartsWith("$ cd"))
			{
				pwd = pwd.Children.First(x => x.Type == NodeType.Directory && x.Name == command.Split(" ")[2]);
			}
			else if (command.StartsWith("$ ls"))
			{
			}

			else if (command.StartsWith("dir"))
			{
				Node newDir = new Node()
				{
					Name = command.Split(" ")[1],
					Type = NodeType.Directory,
					Parent = pwd
				};

				pwd.Children.Add(newDir);
				allDirs.Add(newDir);
			}
			else
			{
				int fileSize = Convert.ToInt32(command.Split(" ")[0]);
				pwd.Children.Add(new Node()
				{
					Name = command.Split(" ")[1],
					Type = NodeType.File,
					Parent = pwd,
					Size = fileSize
				});

				pwd.Size += fileSize;
				sizePropagator = pwd;

				while (sizePropagator.Parent != null)
				{
					sizePropagator = sizePropagator.Parent;
					sizePropagator.Size += fileSize;
				}
			}
		}

		return allDirs;
	}

	public static string Run1()
	{
		var allDirs = StructureBuilder();

		return allDirs.Where(x => x.Size < 100000).Sum(x => x.Size).ToString();
	}

	public static string Run2()
	{
		var allDirs = StructureBuilder();
		int diskCapacity = 70000000;
		int updateSize = 30000000;
		int freeSpace = diskCapacity - allDirs.First(x => x.Name == "\\").Size;
		int spaceNeeded = updateSize - freeSpace;

		var folderLeastEnough = allDirs.Where(x => x.Size >= spaceNeeded).Min(x => x.Size);
		return folderLeastEnough.ToString();
	}
}