﻿namespace AoC2022.Days.Models;

public class Node
{
	public Node()
	{
		Children = new List<Node>();
	}

	public Node(string name)
	{
		Name = name;
		Children = new List<Node>();
	}

	public string Name { get; set; }
	public int Size { get; set; }
	public NodeType Type { get; set; }
	public Node Parent { get; set; }
	public List<Node> Children { get; set; }
}