﻿namespace AoC2022.Days.Models;

public enum NodeType
{
	File = 1,
	Directory = 2
}