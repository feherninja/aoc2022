﻿using AoC2022.Engine;

namespace AoC2022.Days;

public static class Day6
{
	private static readonly List<string> Input1 = Loader.LoadInput("1");
	private static readonly List<string> InputSample = Loader.LoadInput("Sample");

	public static string Run1()
	{
		string input = Input1[0];

		foreach (var iterator in Enumerable.Range(0, input.Length - 5))
		{
			var digit4 = String.Join("",input.Skip(iterator).Take(4));

			if (digit4.Distinct().Count() == 4)
			{
				return (input.IndexOf(digit4)+4).ToString();
			}
		}

		return string.Empty;
	}

	public static string Run2()
	{
		string input = Input1[0];
		int markerLength = 14;
		
		foreach (var iterator in Enumerable.Range(0, input.Length - 5))
		{
			var digit4 = String.Join("",input.Skip(iterator).Take(markerLength));

			if (digit4.Distinct().Count() == markerLength)
			{
				return (input.IndexOf(digit4)+markerLength).ToString();
			}
		}

		return string.Empty;
	}
}