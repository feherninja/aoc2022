﻿using AoC2022.Engine;

namespace AoC2022.Days;

public static class DayTemplate
{
	private static readonly List<string> Input = Loader.LoadInput("");
	private static readonly List<string> InputExample = Loader.LoadInput("Example");

	public static string Run1()
	{
		return string.Empty;
	}

	public static string Run2()
	{
		return string.Empty;
	}
}