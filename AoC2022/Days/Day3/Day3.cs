﻿using AoC2022.Engine;

namespace AoC2022.Days;

public static class Day3
{
	private static readonly List<string> Input1 = Loader.LoadInput("1");
	private static readonly List<string> InputExample = Loader.LoadInput("Example");

	public static string Run1()
	{
		List<char> duplicates = new();

		foreach (var rucksack in Input1)
		{
			var susChars = rucksack[0..((rucksack.Length / 2))].Where(x => rucksack[(rucksack.Length / 2)..].Contains(x));

			foreach (var c in susChars.Distinct())
			{
				duplicates.Add(c);
			}
		}

		var sum = duplicates.Select(x => char.IsUpper(x) ? ((int) x) - 38 : ((int) x) - 96).Sum();
		return sum.ToString();
	}

	public static string Run2()
	{
		var groupsOf3 = Input1.Chunk(3);
		List<char> badges = new();

		foreach (var group in groupsOf3)
		{
			var badge = group[0].FirstOrDefault(x => group[1].Contains(x) && group[2].Contains(x));
			badges.Add(badge);
		}

		var sum = badges.Select(x => char.IsUpper(x) ? ((int) x) - 38 : ((int) x) - 96).Sum();
		return sum.ToString();
	}
}