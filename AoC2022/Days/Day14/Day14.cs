﻿using AoC2022.Engine;

namespace AoC2022.Days;

public class Rock
{
	public int X { get; set; }
	public int Y { get; set; }
}

public static class Day14
{
	private static readonly List<string> Input = Loader.LoadInput("");
	private static readonly List<string> InputExample = Loader.LoadInput("Example");

	public static string Run1()
	{
		var input = Input;
		List<Rock> rocks = BuildCavern(input);
		List<Rock> sand = new List<Rock>();
		int deadLine = rocks.Max(x => x.Y) + 1;

		//DisplayCavern(rocks, sand);

		while (true)
		{
			var grain = new Rock() { X = 500, Y = 0 };

			while (true)
			{
				//DisplayCavern(rocks, sand, grain);

				if (grain.Y == deadLine)
				{
					return sand.Count().ToString();
				}

				if (!rocks.Union(sand).Any(x => x.Y == grain.Y + 1 && x.X == grain.X))
				{
					grain.Y++;
				}
				else if (!rocks.Union(sand).Any(x => x.Y == grain.Y + 1 && x.X == grain.X - 1))
				{
					grain.Y++;
					grain.X--;
				}
				else if (!rocks.Union(sand).Any(x => x.Y == grain.Y + 1 && x.X == grain.X + 1))
				{
					grain.Y++;
					grain.X++;
				}
				else
				{
					sand.Add(grain);
					break;
				}
			}
		}
	}

	public static string Run2()
	{
		var input = Input;
		List<Rock> rocks = BuildCavern(input);
		List<Rock> sand = new List<Rock>();
		int deadLine = rocks.Max(x => x.Y) + 1;

		//DisplayCavern(rocks, sand);

		while (true)
		{
			var grain = new Rock() { X = 500, Y = 0 };

			if (sand.Count % 100 == 0)

				while (true)
				{
					//DisplayCavern(rocks, sand, grain);

					if (grain.Y == deadLine)
					{
						//return sand.Count().ToString();
						sand.Add(grain);
						break;
					}

					if (!rocks.Any(x => x.Y == grain.Y + 1 && x.X == grain.X) && !sand.Any(x => x.Y == grain.Y + 1 && x.X == grain.X))
					{
						grain.Y++;
					}
					else if (!rocks.Any(x => x.Y == grain.Y + 1 && x.X == grain.X - 1) && !sand.Any(x => x.Y == grain.Y + 1 && x.X == grain.X - 1))
					{
						grain.Y++;
						grain.X--;
					}
					else if (!rocks.Any(x => x.Y == grain.Y + 1 && x.X == grain.X + 1) && !sand.Any(x => x.Y == grain.Y + 1 && x.X == grain.X + 1))
					{
						grain.Y++;
						grain.X++;
					}
					else
					{
						sand.Add(grain);

						if (grain.Y == 0)
						{
							//DisplayCavern(rocks, sand, grain);
							return sand.Count.ToString();
						}

						break;
					}
				}
		}
	}

	private static void DisplayCavern(List<Rock> rocks, List<Rock> sand, Rock grain = null)
	{
		var minHorizontal = rocks.Union(sand).Min(x => x.X) - 2;
		var maxHorizontal = rocks.Union(sand).Max(x => x.X) + 2;
		var minVertical = 0;
		var maxVertical = rocks.Union(sand).Max(x => x.Y) + 4;

		Console.Clear();

		for (int i = minVertical; i < maxVertical; i++)
		{
			for (int j = minHorizontal; j < maxHorizontal; j++)
			{
				if (grain?.X == j && grain?.Y == i)
				{
					Console.ForegroundColor = ConsoleColor.Yellow;
					Console.Write("o");
					Console.ForegroundColor = ConsoleColor.White;
				}

				else if (rocks.Any(x => x.X == j && x.Y == i))
				{
					Console.Write("#");
				}
				else if (sand.Any(x => x.X == j && x.Y == i))
				{
					Console.Write("o");
				}
				else
				{
					Console.Write(".");
				}
			}

			Console.WriteLine();
		}
	}

	public static List<Rock> BuildCavern(List<string> input)
	{
		List<Rock> rocks = new List<Rock>();

		foreach (var rockModel in input)
		{
			var rockCorners = rockModel.Split(" -> ");

			for (int i = 1; i < rockCorners.Length; i++)
			{
				int prevHorizontal = Convert.ToInt32(rockCorners[i - 1].Split(",")[0]);
				int prevVertical = Convert.ToInt32(rockCorners[i - 1].Split(",")[1]);
				int currentHorizontal = Convert.ToInt32(rockCorners[i].Split(",")[0]);
				int currentVertical = Convert.ToInt32(rockCorners[i].Split(",")[1]);

				// horizontal line
				if (prevVertical == currentVertical)
				{
					foreach (var horizontalCoord in GetNumbersBetween(currentHorizontal, prevHorizontal))
					{
						if (!rocks.Any(x => x.X == horizontalCoord && x.Y == prevVertical))
						{
							Rock currentRock = new Rock()
							{
								X = horizontalCoord,
								Y = prevVertical
							};

							rocks.Add(currentRock);
						}
					}
				}

				// vertial line
				if (prevHorizontal == currentHorizontal)
				{
					foreach (var verticalCoord in GetNumbersBetween(prevVertical, currentVertical))
					{
						if (!rocks.Any(x => x.X == prevHorizontal && x.Y == verticalCoord))
						{
							Rock currentRock = new Rock()
							{
								X = prevHorizontal,
								Y = verticalCoord
							};

							rocks.Add(currentRock);
						}
					}
				}
			}
		}

		return rocks;
	}

	public static List<int> GetNumbersBetween(int a, int b)
	{
		if (a > b)
		{
			(a, b) = (b, a);
		}

		var numbers = new List<int>();

		for (int i = a; i <= b; i++)
		{
			numbers.Add(i);
		}

		return numbers;
	}
}