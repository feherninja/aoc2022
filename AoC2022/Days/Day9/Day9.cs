﻿using AoC2022.Engine;

namespace AoC2022.Days;

public class Day9
{
	private static readonly List<string> Input = Loader.LoadInput("");
	private static readonly List<string> InputExample = Loader.LoadInput("Example");
	private static readonly List<string> InputLongExample = Loader.LoadInput("LongExample");
	private Point Head { get; set; }
	private Point Tail { get; set; }
	List<Point> Visited = new List<Point>();
	private List<Point> LongRope = new List<Point>();

	public Day9()
	{
		Head = new Point() { X = 0, Y = 0 };
		Tail = new Point() { X = 0, Y = 0 };
	}

	public string Run1()
	{
		var input = Input;

		foreach (var command in input)
		{
			var direction = command.Split(" ")[0];
			var distance = command.Split(" ")[1];

			foreach (var iterator in Enumerable.Range(0, Convert.ToInt32(distance)))
			{
				moveHead(Head, direction);
				matchTail(Tail, Head);
				markCoordinate(Tail);
			}
		}

		return Visited.Count.ToString();
	}

	public string Run2()
	{
		var input = Input;
		int ropeLenght = 10;

		foreach (var iterator in Enumerable.Range(0, ropeLenght))
		{
			LongRope.Add(new Point());
		}

		foreach (var command in input)
		{
			var direction = command.Split(" ")[0];
			var distance = command.Split(" ")[1];

			foreach (var iterator in Enumerable.Range(0, Convert.ToInt32(distance)))
			{
				moveHead(LongRope.First(), direction);

				for (int i = 1; i < LongRope.Count; i++)
				{
					matchTail(LongRope[i], LongRope[i - 1]);
				}
				
				//DisplayRope(LongRope, 5, 11);
				markCoordinate(LongRope.Last());
			}
		}
		
		return Visited.Count.ToString();
	}

	private void DisplayRope(List<Point> rope, int offsetX = 0, int offsetY = 0)
	{
		Console.Clear();
		List<Point> marks = new List<Point>();
		int width = 27;
		int heigth = 22;

		for (int i = 0; i < rope.Count; i++)
		{
			marks.Add(new Point()
			{
				Name = i == 0 ? "H" : i.ToString(),
				X = rope[i].X,
				Y = rope[i].Y
			});
		}

		for (int i = 0; i < heigth - 1; i++)
		{
			for (int j = 0; j < width - 1; j++)
			{
				char displayChar = '.';
				if (i == offsetX && j == offsetY) displayChar = 's';

				var knotOnPoint = marks.FirstOrDefault(x => x.X+offsetX == i && x.Y+offsetY == j);
				if (knotOnPoint != null) displayChar = knotOnPoint.Name[0];
				Console.Write(displayChar);
			}

			Console.WriteLine();
		}
	}

	private void markCoordinate(Point target)
	{
		if (!Visited.Any(x => x.X == target.X && x.Y == target.Y))
		{
			Visited.Add(new Point() { X = target.X, Y = target.Y });
		}
	}

	private void matchTail(Point what, Point toWhat)
	{
		// Touching 
		if (-1 < Math.Abs(toWhat.X - what.X) && Math.Abs(toWhat.X - what.X) <= 1 &&
		    -1 < Math.Abs(toWhat.Y - what.Y) && Math.Abs(toWhat.Y - what.Y) <= 1)
		{
			return;
		}

		// Up - Down
		if (toWhat.Y == what.Y)
		{
			if (toWhat.X > what.X) what.X++;
			else what.X--;

			return;
		}

		// Left - Right
		if (toWhat.X == what.X)
		{
			if (toWhat.Y > what.Y) what.Y++;
			else what.Y--;

			return;
		}

		// Diagonal

		if (toWhat.X > what.X) what.X++;
		else what.X--;

		if (toWhat.Y > what.Y) what.Y++;
		else what.Y--;

		return;
	}

	private void moveHead(Point movingPoint, string direction)
	{
		switch (direction)
		{
			case "U":
				movingPoint.X++;
				break;
			case "D":
				movingPoint.X--;
				break;
			case "L":
				movingPoint.Y--;
				break;
			case "R":
				movingPoint.Y++;
				break;
		}
	}
}