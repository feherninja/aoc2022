﻿namespace AoC2022.Days;

public class Point
{
	public Point()
	{
		X = 0;
		Y = 0;
	}

	public string Name { get; set; }
	public int X { get; set; }
	public int Y { get; set; }
}