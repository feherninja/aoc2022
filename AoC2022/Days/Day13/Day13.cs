﻿using AoC2022.Engine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AoC2022.Days;

public static class Day13
{
	private static readonly List<string> Input = Loader.LoadInput("");
	private static readonly List<string> InputExample = Loader.LoadInput("Example");
	
	private static int Iterator = 0;

	public static string Run1()
	{
		var input = Input;
		List<int> rightIndexes = new List<int>();
		int iterator = 0;
		List<string[]> maybeRights = new List<string[]>();

		foreach (var compareTriad in input.Chunk(3))
		{
			iterator++;

			JArray objectL = JsonConvert.DeserializeObject(compareTriad[0]) as JArray;
			JArray objectR = JsonConvert.DeserializeObject(compareTriad[1]) as JArray;

			var rightOrder = Compare(objectL, objectR);
			
			if (rightOrder.Value)
			{
				rightIndexes.Add(iterator);
			}
			else
			{
				maybeRights.Add(compareTriad);
			}
		}

		foreach (var rightIndex in rightIndexes)
		{
			Console.WriteLine(rightIndex.ToString());
		}

		return rightIndexes.Sum().ToString();
	}

	public static string Run2()
	{
		var input = Input.Where(x => !string.IsNullOrEmpty(x)).ToList();
		input.Add("[[2]]");
		input.Add("[[6]]");

		input.Sort((a, b) => Comparer(JsonConvert.DeserializeObject(a) as JArray, JsonConvert.DeserializeObject(b) as JArray).Value);
		input.Reverse();

		var magicLocator1 = input.IndexOf("[[2]]") + 1;
		var magicLocator2 = input.IndexOf("[[6]]") + 1;
		return (magicLocator1 * magicLocator2).ToString();
	}

	private static bool? Compare(JArray objectL, JArray objectR)
	{
		if (objectL.Children().Count() == 0 && objectR.Children().Count() != 0)
		{
			return true;
		}

		if (objectL.Children().Count() != 0 && objectR.Children().Count() == 0)
		{
			return false;
		}

		var iterationLimit = new List<JArray>() { objectL, objectR }.Select(x => x.Children().Count()).Min();

		for (int i = 0; i < iterationLimit; i++)
		{
			if (objectL[i].GetType() == new JValue(1).GetType() && objectR[i].GetType() == new JValue(1).GetType())
			{
				if (objectL[i].Value<int>() < objectR[i].Value<int>())
				{
					return true;
				}

				if (objectL[i].Value<int>() > objectR[i].Value<int>())
				{
					return false;
				}
			}

			else if (objectL[i].GetType() == new JArray().GetType() && objectR[i].GetType() == new JArray().GetType())
			{
				bool? subCompare = Compare(objectL[i] as JArray, objectR[i] as JArray);

				if (subCompare is not null)
				{
					return subCompare.Value;
				}
			}

			else if (objectL[i].GetType() == new JArray().GetType() && objectR[i].GetType() == new JValue(1).GetType())
			{
				bool? subCompare = Compare(objectL[i] as JArray, new JArray() { objectR[i] });

				if (subCompare is not null)
				{
					return subCompare.Value;
				}
			}

			else if (objectL[i].GetType() == new JValue(1).GetType() && objectR[i].GetType() == new JArray().GetType())
			{
				bool? subCompare = Compare(new JArray() { objectL[i] }, objectR[i] as JArray);

				if (subCompare is not null)
				{
					return subCompare.Value;
				}
			}
		}

		if (objectL.Children().Count() > objectR.Children().Count())
		{
			return false;
		}

		return true;
	}

	private static int? Comparer(JArray objectL, JArray objectR)
	{
		if (objectL.Children().Count() == 0 && objectR.Children().Count() != 0)
		{
			return 1;
		}

		if (objectL.Children().Count() != 0 && objectR.Children().Count() == 0)
		{
			return -1;
		}

		var iterationLimit = new List<JArray>() { objectL, objectR }.Select(x => x.Children().Count()).Min();

		for (int i = 0; i < iterationLimit; i++)
		{
			if (objectL[i].GetType() == new JValue(1).GetType() && objectR[i].GetType() == new JValue(1).GetType())
			{
				if (objectL[i].Value<int>() < objectR[i].Value<int>())
				{
					return 1;
				}

				if (objectL[i].Value<int>() > objectR[i].Value<int>())
				{
					return -1;
				}
			}

			else if (objectL[i].GetType() == new JArray().GetType() && objectR[i].GetType() == new JArray().GetType())
			{
				int? subCompare = Comparer(objectL[i] as JArray, objectR[i] as JArray);

				if (subCompare is not null)
				{
					return subCompare.Value;
				}
			}

			else if (objectL[i].GetType() == new JArray().GetType() && objectR[i].GetType() == new JValue(1).GetType())
			{
				int? subCompare = Comparer(objectL[i] as JArray, new JArray() { objectR[i] });

				if (subCompare is not null)
				{
					return subCompare.Value;
				}
			}

			else if (objectL[i].GetType() == new JValue(1).GetType() && objectR[i].GetType() == new JArray().GetType())
			{
				int? subCompare = Comparer(new JArray() { objectL[i] }, objectR[i] as JArray);

				if (subCompare is not null)
				{
					return subCompare.Value;
				}
			}
		}

		if (objectL.Children().Count() > objectR.Children().Count())
		{
			return -1;
		}

		return 1;
	}
}