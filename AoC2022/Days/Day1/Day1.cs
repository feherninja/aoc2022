﻿using AoC2022.Engine;

namespace AoC2022.Days;

public static class Day1
{
	private static readonly List<string> Input1 = Loader.LoadInput("1");

	public static string Run1()
	{
		double max = 0;
		double current = 0;

		for (int i = 0; i < Input1.Count; i++)
		{
			if (string.IsNullOrEmpty(Input1[i]))
			{
				if (current > max) max = current;
				current = 0;
				continue;
			}

			current += Convert.ToInt32(Input1[i]);
		}

		return max.ToString();
	}

	public static string Run2()
	{
		List<double> calories = new();
		double current = 0;

		for (int i = 0; i < Input1.Count; i++)
		{
			if (string.IsNullOrEmpty(Input1[i]))
			{
				calories.Add(current);
				current = 0;
				continue;
			}

			current += Convert.ToInt32(Input1[i]);
		}

		return calories.OrderByDescending(x => x).Take(3).Sum().ToString();
	}
}