﻿namespace AoC2022.Engine;

public class Loader
{
	public static List<string> LoadInput(string postfix,  [System.Runtime.CompilerServices.CallerFilePath] string callerFilePath ="")
	{
		var path = $"{string.Join("\\",callerFilePath.Split("\\")[0..^1])}\\Input{postfix}.txt";
		string[] lines = File.ReadAllLines(path);  

		return lines.ToList();
	}
}